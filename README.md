# Proportion of juveniles

Research project seeking to provide guidance for the design of age counts of a migratory goose species, by identifying which factors need to be taken into account when collecting data. 

## How to cite this material: 
https://doi.org/10.5281/zenodo.7223565

